%global apiver 1.6
%global gssdp_version 1.6.2

Name:          gupnp
Version:       1.6.6
Release:       1
Summary:       UPnP devices & control points creation framework
License:       LGPLv2+
URL:           https://www.gupnp.org/
Source0:       https://download.gnome.org/sources/%{name}/1.6/%{name}-%{version}.tar.xz
Patch0001:     gupnp-1.6.6-libxml2-2.12-includes.patch

BuildRequires: gssdp-devel >= %{gssdp_version} gtk-doc gobject-introspection-devel docbook-style-xsl
BuildRequires: libsoup-devel libxml2-devel libuuid-devel vala meson cmake gi-docgen
Requires:      dbus
Requires:      gssdp%{?_isa} >= %{gssdp_version}

%description
GUPnP is an elegant, object-oriented open source framework for creating UPnP
devices and control points, written in C using GObject and libsoup. The GUPnP
API is intended to be easy to use, efficient and flexible. It provides the same
set of features as libupnp,but shields the developer from most of UPnP's internals.

%package       devel
Summary:       Libraries/include files package for gupnp
Requires:      %{name} = %{version}-%{release}

%description   devel
Libraries/include files for development with gupnp.

%package       help
Summary:       Help documentations for gupnp
Requires:      %{name} = %{version}-%{release}
BuildArch:     noarch
Provides:      %{name}-docs = %{version}-%{release}
Obsoletes:     %{name}-docs < %{version}-%{release}

%description   help
This package contains help file and developer documentation for gupnp.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
  -Dcontext_manager=network-manager \
  -Dgtk_doc=true \
  -Dexamples=false \
  %{nil}
%meson_build

%install
%meson_install
%delete_la

cp %{_libdir}/libgupnp-1.2.so.* %{buildroot}%{_libdir}/

%check
%meson_test

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING	
%{_libdir}/libgupnp-%{apiver}.so.0*
%{_libdir}/libgupnp-1.2.so.1*
%{_libdir}/girepository-1.0/GUPnP-%{apiver}.typelib

%files devel
%{_bindir}/gupnp-binding-tool-%{apiver}
%{_includedir}/gupnp-%{apiver}/
%{_libdir}/libgupnp-%{apiver}.so
%{_libdir}/libgupnp-1.2.so.1
%{_libdir}/pkgconfig/gupnp-%{apiver}.pc
%{_datadir}/gir-1.0/GUPnP-%{apiver}.gir
%{_datadir}/vala/vapi/gupnp*

%files help
%doc AUTHORS README.md
%{_mandir}/man1/gupnp-binding-tool-*
%{_docdir}/gupnp-%{apiver}/

%changelog
* Fri Feb 23 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 1.6.6-1
- update to 1.6.6

* Sun Feb 18 2024 liyanan <liyanan61@h-partners.com> - 1.6.4-2
- Fix build error

* Mon Aug 07 2023 xu_ping <707078654@qq.com> - 1.6.4-1
- Upgrade version to 1.6.4

* Thu Jan 18 2023 xingxing <xingxing@xfusion.com> - 1.4.3-2
- Fix python script shebang

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.4.3-1
- Update to 1.4.3

* Wed Jul 14 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 1.2.4-2
- Add missing BuildRequires version: glib2-devel >= 2.66

* Mon Jun 7 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.2.4-1
- Upgrade to 1.2.4
- Update Version, Release, Source0, BuildRequires
- Delete sed operation which existed in this version
- Add patch for fix CVE-2021-33516
- Update stage 'prep', 'build', 'install', 'check', 'files'

* Fri Oct 25 2019 Alex Chao <zhaolei746@huawei.com> - 1.0.3-2
- Package init
